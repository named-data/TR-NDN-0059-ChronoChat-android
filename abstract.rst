.. environment:: abstract

                 In this report I discuss my Master's capstone project,
                 *ChronoChat-android*, an implementation of the ChronoChat
                 instant message application for Android mobile devices
                 :cite:`chronochat-android`. ChronoChat-android allows
                 communication in a chatroom cooperatively hosted by ChronoChat
                 clients. As in other ChronoChat implementations, chatroom
                 participants may be connected via an NDN hub;
                 ChronoChat-android also supports ad-hoc communication between
                 mobile devices running NFD-android with Wi-Fi Direct support
                 :cite:`nfd-android`. This report will review the design and
                 features of ChronoChat-android, focusing on aspects that are of
                 particular importance to the Android platform. It will also
                 compare ChronoChat-android to another Android app---NDN
                 Whiteboard :cite:`ndn-whiteboard`---and to the web-based
                 ChronoChat-js implementation :cite:`chronochat-js`.
