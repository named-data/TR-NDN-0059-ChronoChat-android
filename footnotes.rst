.. [#thread_safety]

   It should be noted that despite such warnings, I did not encounter
   any obvious thread-safety bugs in my testing of NDN Whiteboard or alternative
   threading arrangements in ChronoChatService.)

.. [#timestamp]

   ChronoChat-android trusts the sender-specified message timestamp.
